﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UTH.Extension;

namespace UTH.Core
{
    public class TimeCore
    {
        public const string DEF_URL_TIME = "https://satriverstudio.com/helper/getTime.php";
        public static string URL_TIME
        {
            get
            {
                try
                {
                    if (string.IsNullOrEmpty(TimeSettings.ServerUrl))
                    {
                        Debug.Log("server url return empty, using default url.");
                        return DEF_URL_TIME;
                    }
                }
                catch (System.Exception)
                {
                    Debug.Log("load file time data setting return error, please create scriptable object inside resources folder first, using def url now");
                    return DEF_URL_TIME;
                }

                return TimeSettings.ServerUrl;
            }
        }

        private static TimeDataSettings timeSettings;
        public static TimeDataSettings TimeSettings
        {
            get
            {
                try
                {
                    if (timeSettings == null)
                    {
                        var item = Resources.Load<TimeDataSettings>("Default Time Helper Settings");
                        timeSettings = item;
                    }

                }
                catch (System.Exception e)
                {
                    Debug.Log("error : " + e.Message);
                }

                return timeSettings;
            }
        }


        public static string currentDate;
        public static string currentTime;

        private static string timeData;
        private static UnityWebRequest webRequest;

        private async static Task CreateWebRequest(string url = DEF_URL_TIME, RequestType type = RequestType.GET)
        {
            webRequest = new UnityWebRequest(url, type.ToString());
            webRequest.downloadHandler = new DownloadHandlerBuffer();
            var op = webRequest.SendWebRequest();

            while (!op.isDone)
                await Task.Yield();
        }

        private static bool CheckRequestValid()
        {
#if UNITY_2019
            return (!webRequest.isNetworkError || !webRequest.isHttpError);
#elif UNITY_2019_4_OR_NEWER
        return (webRequest.result == UnityWebRequest.Result.Success);
#endif
        }

        /// <summary>
        /// Get current server date.
        /// NOTE : the server must print text with format "date/time".
        /// </summary>
        /// <param name="url">url to retrive current date</param>
        /// <returns></returns>
        public async static Task<string> GetServerDate(string url = DEF_URL_TIME)
        {
            await CreateWebRequest(url);

            if (!webRequest.isDone)
                await Task.Yield();

            if (CheckRequestValid() == false)
            {
                Debug.LogError("Failed when get from url.");
                return "null";
            }

            timeData = webRequest.downloadHandler.text;
            string[] _results = timeData.Split('/');

            currentDate = _results[0];
            return currentDate.ToString();
        }

        /// <summary>
        /// Get current server time.
        /// NOTE : the server must print text with format "date/time".
        /// </summary>
        /// <param name="url">url to retrive current time</param>
        /// <returns></returns>
        public async static Task<string> GetServerTime(string url = DEF_URL_TIME)
        {
            await CreateWebRequest(url);

            if (!webRequest.isDone)
                await Task.Yield();

            if (CheckRequestValid() == false)
            {
                Debug.LogError("Failed when get from url.");
                return "null";
            }

            timeData = webRequest.downloadHandler.text;
            string[] _results = timeData.Split('/');

            currentTime = _results[1];
            return currentTime.ToString();
        }
    }


    public enum RequestType
    {
        GET = 0,
        POST = 1,
        PUT = 2
    }

}
