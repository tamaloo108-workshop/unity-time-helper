﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UTH.Extension
{
    [CreateAssetMenu(fileName = "Default Time Helper Settings", menuName = "Unity Time Helper/Create new time Settings")]
    public class TimeDataSettings : ScriptableObject
    {
        public string ServerUrl;
        public bool UseLocalTimePrediction;
    }
}
